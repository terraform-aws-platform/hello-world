describe('Hello World App', () => {
    it('should display hello world message', () => {
      // Visit the Angular app's URL
      cy.visit('http://localhost:4200');
  
      // Assert that the page contains the expected text
      cy.contains('app-root h1', 'Hello, world!');
    });
  });
  